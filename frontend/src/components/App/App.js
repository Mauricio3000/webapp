import React from 'react';
import './App.css';
import Box from '../Box/Box';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        Behold, a WebApp!
      </header>

      <Box title="Hello!" body="This is a component." />
      <Box title="What's new?" body="This is another component." />

    </div>
  );
}

export default App;