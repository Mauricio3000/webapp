import React from 'react';


function Box(props) {
  return (
    <div className="BoxComponent">
        <h1>{props.title}</h1>
        <p>{props.body}</p>
    </div>
  );
}

export default Box;