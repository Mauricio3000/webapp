import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import Box from "./Box";

let container = null;

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders props", () => {
  let titleStr = "Hello";
  let bodyStr = "What's up?";

  act(() => {
    render(<Box title={titleStr}/>, container);
  });
  expect(container.textContent).toBe(titleStr);

  act(() => {
    render(<Box body={bodyStr} />, container);
  });
  expect(container.textContent).toBe(bodyStr);

  act(() => {
    render(<Box title={titleStr} body={bodyStr}/>, container);
  });
  expect(container.textContent).toBe(titleStr + bodyStr);
});
