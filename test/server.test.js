// describe('Sample Test', () => {
//     it('should test that true === true', () => {
//       expect(true).toBe(true)
//     })
//   })

const request = require('supertest');
const app = require('../server.js');
let server = null;

beforeEach(() => {
  server = app.listen({ port: 8080 }, () => {})
});

afterEach(() => {
  server.close();
})
  
describe('Basic tests', () => {
  it('is running', async () => {
    const res = await request(app).get('/')

    expect(res.statusCode).toEqual(200);
  })
})
  