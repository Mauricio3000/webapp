var app = require('../server');

const port = 8080;

app.listen({ port: port }, () => {
    console.log("Server ready at http://localhost:" + port)
})