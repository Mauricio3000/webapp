const express = require("express")
const cors = require("cors")

//--- Express setup
const app = express();
app.use(cors())
app.use(express.static('client'))

module.exports = app;