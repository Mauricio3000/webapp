
#
# 1. Build the docker image: 
# >> docker build -t tag .
#
# 2. Run an instance of the image. Bind exposed port to 300 and run in detahced mode.
# >> docker run -p 80:80 -d tag
#
# 3. Get into running container
# >> docker exec -it <HASH> /bin/bash


FROM node:latest

WORKDIR /opt/server
COPY ./ ./

RUN npm install

EXPOSE 8080

CMD ["npm", "start"]